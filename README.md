# SWPedia

SWPedia is a simple app for exploring the Star Wars universe 🌌


To run the project clone it with

```
git clone git@bitbucket.org:vojtech_houzvicka/swpedia.git
```

then inside `swpedia/` run

```
npm install
npm run serve
```

to serve the app 🚀

---

## Challenges & TODOs

Completing the challenge took  around 10 hours. Ordering of the table presented a challenge, since the SWApi does not support ordering. Since the results are paginated, we need to first fetch all pages to be able to order them locally. For production storybook or different component library should be added and also all atoms and molecules should be unit tested.

---

## The assignment

>Your mission, should you choose to accept it, is to create a one-page application with a list of
people and the details about their related home planet.
The list of people and the information related to a planet can be accessed using the swapi
>
>api: SWAPI api details: https://swapi.dev/
>
>Wireframe description:
> - [x] The candidate should implement a table that contains a list of users with the following columns:
>
>   - [x] Name
>   - [x] Height
>   - [x] Mass
>   - [x] Created
>   - [x]  Edited
>   - [x]  Planet Name
>
> - [x] When the user clicks on the planet name link a popup is displayed showing the following
    information regarding the planet:
>
>   - [x]  Name
>   - [x]  Diameter
>   - [x]  Climate
>   - [x]  Population
>   - [x] The user should be able to sort the table by each column
>   - [x] The user should also be able to filter by searching the person’s name.
>
>Stack
> - [x] ES6.
> - [x] Webpack
> - [x] Vue/vuex
> - [ ] vue router
