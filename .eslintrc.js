module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    'plugin:vue/strongly-recommended',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'eol-last': 'error',
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': process.env.VUE_APP_ENV !== 'local' ? 'error' : 'off',
    'no-await-in-loop': 'off',
    'no-console': 'warn',
    'no-debugger': process.env.VUE_APP_ENV !== 'local' ? 'error' : 'off',
    'no-param-reassign': 'off',
    'no-underscore-dangle': 'off',
    'no-use-before-define': 'off',
    'jsdoc/check-alignment': 'error', // Recommended
    'jsdoc/check-indentation': 'error',
    'jsdoc/check-param-names': 'error', // Recommended
    'jsdoc/check-syntax': 'error',
    'jsdoc/check-tag-names': 'error', // Recommended
    'jsdoc/check-types': 'error', // Recommended
    'jsdoc/implements-on-classes': 'warn', // Recommended
    'jsdoc/match-description': 'off',
    'jsdoc/newline-after-description': 'error', // Recommended
    'jsdoc/no-types': 'off',
    'jsdoc/no-undefined-types': 'warn', // Recommended
    'jsdoc/require-description': 'error',
    'jsdoc/require-description-complete-sentence': 'off',
    'jsdoc/require-example': 'off',
    'jsdoc/require-hyphen-before-param-description': 'off',
    'jsdoc/require-jsdoc': 'error', // Recommended
    'jsdoc/require-param': 'error', // Recommended
    'jsdoc/require-param-description': 'off', // Recommended
    'jsdoc/require-param-name': 'error', // Recommended
    'jsdoc/require-param-type': 'error', // Recommended
    'jsdoc/require-returns': 'error', // Recommended
    'jsdoc/require-returns-check': 'error', // Recommended
    'jsdoc/require-returns-description': 'off', // Recommended
    'jsdoc/require-returns-type': 'error', // Recommended
    'jsdoc/valid-types': 'warn', // Recommended
    'vue/attributes-order': 'error',
    'vue/component-tags-order': 'error',
    'vue/no-bare-strings-in-template': 'off', // Disabling for swpedia only, but would use in prod
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        'src/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.json', '.vue'],
      },
    },
  },
  plugins: ['jsdoc'],
};
