import './styles/quasar.scss';
import iconSet from 'quasar/icon-set/ionicons-v4';
import '@quasar/extras/roboto-font/roboto-font.css';
import '@quasar/extras/ionicons-v4/ionicons-v4.css';

// To be used on app.use(Quasar, { ... })
export default {
  config: {},
  plugins: {
  },
  iconSet,
};
