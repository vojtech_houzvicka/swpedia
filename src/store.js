/**
 * For simplicity this is all in one file, in bigger project mutations, actions and getters
 * would live in their own files
 */

import { createStore } from 'vuex';
import apiService from './services/ApiService';
import creaturesService from './services/CreaturesService';

const state = {
  planets: {},
  loadingCreatures: false,
  loadingCreaturesPromise: null,
  creatures: [],
};

const mutations = {
  addPlanet(stateObj, url) {
    stateObj.planets[url] = apiService.fetchPlanet(url);
  },
  setCreatures(stateObj, creatures) {
    stateObj.creatures = creatures;
  },
  setLoadingCreatures(stateObj, newVal) {
    stateObj.loadingCreatures = newVal;
  },
  setLoadingCreaturesPromise(stateObj, newVal) {
    stateObj.loadingCreaturesPromise = newVal;
  },
};

const actions = {
  addPlanet: ({ commit }, url) => commit('addPlanet', url),
  searchCreatures: async ({ commit }, query) => {
    // Cancel any previous searches
    if (state.loadingCreaturesPromise) {
      state.loadingCreaturesPromise.canceled = true;
    }

    const creaturesPromise = creaturesService.fetchCreatures(query);
    commit('setLoadingCreaturesPromise', creaturesPromise);
    commit('setLoadingCreatures', true);

    creaturesPromise.then((creatures) => {
      if (creaturesPromise.canceled) {
        return;
      }

      commit('setCreatures', creatures);
      commit('setLoadingCreaturesPromise', null);
      commit('setLoadingCreatures', false);
    });
  },
};

export default createStore({
  actions,
  mutations,
  state,
});
