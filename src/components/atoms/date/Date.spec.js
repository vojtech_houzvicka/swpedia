import { mount } from '@vue/test-utils';

import Date from './Date.vue';

const factory = (propsData) => mount(Date, {
  stubs: [
    'q-tooltip',
  ],
  propsData,
});

describe('Date atom', () => {
  it('renders date component correctly', () => {
    const date = '2014-12-10T15:59:50.509000Z';
    const wrapper = factory({ date });

    expect(wrapper.find('span').exists()).toBeTruthy();
    expect(wrapper.find('q-tooltip').exists()).toBeTruthy();
    expect(wrapper.find('span').text())
      .toEqual(`2014/12/10 ${date}`);
    expect(wrapper.find('q-tooltip').text())
      .toEqual(date);
  });
});
