import { useStore } from 'vuex';

/**
 * Fetch planet info from SWApi
 *
 * @param {string} url
 * @returns {Promise<Array>}
 */
async function fetchPlanet(url) {
  const store = useStore();

  // Avoid multiple requests to same URL
  if (!(url in store.state.planets)) {
    await store.dispatch('addPlanet', url);
  }

  const response = await store.state.planets[url];
  return response.status === 200
    ? response.data
    : { name: 'Unknown' };
}

const planetService = {
  fetchPlanet,
};

export default planetService;
