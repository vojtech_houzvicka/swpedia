import axios from 'axios';

const http = axios.create();

const API_ROOT = 'https://swapi.dev/api/';

/**
 * Fetch people endpoint
 *
 * @param {string} query
 * @returns {Promise<Array>}
 */
async function fetchPeople(query) {
  return fetchAllPages('people', query);
}

/**
 * Fetch all available pages of a given endpoint
 * Since the API does not support ordering, if we want to support it in the table, we need all
 * results first
 *
 * @param {string} endpoint
 * @param {string} query
 * @returns {Array}
 */
async function fetchAllPages(endpoint, query) {
  const result = [];
  let page = 1;
  let response = await http.get(`${API_ROOT}${endpoint}/?search=${query}`);
  result.push(...response.data.results);

  while (response.status === 200 && response.data.next) {
    page += 1;
    response = await axios.get(`${API_ROOT}${endpoint}/?page=${page}&search=${query}`);
    result.push(...response.data.results);
  }

  return result;
}

/**
 * Fetch planet endpoint (should take planet id in best case, but planet is referenced with URL
 * so we could parse it from there, but in case of different format that wouldn't work)
 *
 * @param {string} url
 * @returns {Promise<Array>}
 */
async function fetchPlanet(url) {
  return http.get(url);
}

const apiService = {
  fetchPeople,
  fetchPlanet,
};

export default apiService;
