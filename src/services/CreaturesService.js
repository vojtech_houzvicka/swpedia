import apiService from './ApiService';

/**
 * Fetch list of creatures from SWApi
 *
 * @param {string} query
 * @returns {Promise<Array>}
 */
async function fetchCreatures(query) {
  return apiService.fetchPeople(query);
}

const creaturesService = {
  fetchCreatures,
};

export default creaturesService;
